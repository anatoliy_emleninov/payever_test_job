"use strict";

const gulp =         require('gulp');
const sass =         require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps =   require('gulp-sourcemaps');
const concat =       require('gulp-concat');
const coffee =       require('gulp-coffee');
const addsrc =       require('gulp-add-src');


const del = require('del');

gulp.task('styles', function() {
    return gulp.src('dev/scss/styles.scss',{since: gulp.lastRun('styles')})
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer({
            browsers:['last 5 version']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('js', function() {
    return gulp.src(['dev/js/**/*.coffee', '!dev/js/init.coffee', '!dev/js/start.coffee', '!dev/js/apps/config/storage/localstorage.coffee'])
        .pipe(sourcemaps.init())
        .pipe(addsrc.prepend('dev/js/apps/config/storage/localstorage.coffee'))
        .pipe(addsrc.prepend('dev/js/init.coffee'))
        .pipe(addsrc.append('dev/js/start.coffee'))
        .pipe(concat('app.js'))
        .pipe(coffee())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/js'));
});

gulp.task('js:vendor', function() {
    return gulp.src([
        'dev/vendor/js/jquery.min.js',
        'dev/vendor/js/bootstrap.min.js',
        'dev/vendor/js/jquery.uri.min.js',
        'dev/vendor/js/url-pattern.js',
        'dev/vendor/js/underscore.min.js',
        'dev/vendor/js/backbone.min.js',
        'dev/vendor/js/backbone.localstorage.min.js',
        'dev/vendor/js/backbone.paginator.js',
        'dev/vendor/js/backbone.select.js',
        'dev/vendor/js/backbone.marionette.js'
    ], {since: gulp.lastRun('js:vendor')})
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/js'));
});

gulp.task('fonts', function() {
    return gulp.src('dev/vendor/fonts/*.*', {since: gulp.lastRun('fonts')})
        .pipe(gulp.dest('./public/fonts'));
});

gulp.task('images', function() {
    return gulp.src('dev/images/*.*', {since: gulp.lastRun('images')})
        .pipe(gulp.dest('./public/images'));
});

gulp.task('clean', function() {
    return del('public');
});

gulp.task('build', gulp.series('clean', gulp.parallel('styles', 'js', 'js:vendor', 'fonts', 'images') ));

gulp.task('watch', function () {
    gulp.watch('dev/scss/*.scss', gulp.series('styles'));
    gulp.watch('dev/js/**/*.coffee', gulp.series('js'));
});

gulp.task('dev', gulp.series('build', 'watch'));

