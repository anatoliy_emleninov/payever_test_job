app.module 'Entities', (Entities, app, Backbone, Marionette, $, _) ->
    Entities.AlbumModel = Backbone.Model.extend(urlRoot: 'albums')
    Entities.configureStorage Entities.AlbumModel
    Entities.AlbumsCollection = Backbone.Collection.extend(
        url: 'albums'
        model: Entities.AlbumModel
    )
    Entities.configureStorage Entities.AlbumsCollection
    albums = undefined

    initAlbums = ->
        albums = new (Entities.AlbumsCollection)([
            {
                id: 1
                images: [
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                ]
                src_thumb: 'public/images/marionette_3.jpg'
                title: 'Title 1'
            }
            {
                id: 2
                images: [
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                ]
                src_thumb: 'public/images/marionette.jpg'
                title: 'Title 2'
            }
            {
                id: 3
                images: [
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                ]
                src_thumb: 'public/images/marionette.jpg'
                title: 'Title 3'
            }
            {
                id: 4
                images: [
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                ]
                src_thumb: 'public/images/marionette.jpg'
                title: 'Title 4'
            }
            {
                id: 5
                images: [
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                ]
                src_thumb: 'public/images/marionette.jpg'
                title: 'Title 5'
            }
            {
                id: 6
                images: [
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette_3.jpg'
                    'public/images/marionette_2.jpg'
                    'public/images/marionette.jpg'
                ]
                src_thumb: 'public/images/marionette.jpg'
                title: 'Title 6'
            }
        ])
        albums.forEach (item) ->
            item.save()
            return
        albums

    API = getAlbumsEntities: ->
        getAlbums = new (Entities.AlbumsCollection)
        getAlbums.fetch()
        if getAlbums.length == 0
            return initAlbums()
        getAlbums
    app.reqres.setHandler 'albums:entities', ->
        API.getAlbumsEntities()
    return