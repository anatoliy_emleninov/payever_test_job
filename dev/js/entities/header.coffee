app.module 'Entities', (Entities, app, Backbone, Marionette, $, _) ->
    Entities.Header = Backbone.Model.extend(initialize: ->
        Backbone.Select.Me.applyTo this
        return
    )
    Entities.HeaderCollection = Backbone.Collection.extend(
        model: Entities.Header
        initialize: (models, options) ->
            Backbone.Select.One.applyTo this, models, options
            return
    )

    initHeaders = ->
        Entities.headers = new (Entities.HeaderCollection)([ {
            name: 'Albums'
            url: 'albums'
            navigationTrigger: 'albums:list'
        } ])
        return

    API = getHeaders: ->
        if Entities.headers == undefined
            initHeaders()
        Entities.headers
    app.reqres.setHandler 'header:entities', ->
        API.getHeaders()
    return