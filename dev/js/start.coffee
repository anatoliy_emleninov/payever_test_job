console.log 'Start'

app.navigate = (route, options = {}) ->
  Backbone.history.navigate route, options
  return

app.getCurrentRoute = ->
  Backbone.history.fragment

app.on 'start', ->
  if Backbone.history
    Backbone.history.start()
    if @getCurrentRoute() == ''
      app.trigger 'albums:list'
  return
app.start()