app.module 'HeaderApp.List', (List, app, Backbone, Marionette, $, _) ->
    List.Header = Marionette.ItemView.extend(
        template: '#header-link'
        tagName: 'li'
        events: 'click a': 'navigate'
        navigate: (e) ->
            e.preventDefault()
            @trigger 'navigate', @model
            return
        onRender: ->
            if @model.selected
                @$el.addClass 'active'
            return
    )
    List.Headers = Marionette.CompositeView.extend(
        template: '#header-template'
        tagName: 'nav'
        className: 'navbar navbar-default'
        childView: List.Header
        childViewContainer: 'ul'
        events: 'click a.navbar-brand': 'brandClicked'
        brandClicked: (e) ->
            e.preventDefault()
            @trigger 'brand:clicked'
            return
    )
    return