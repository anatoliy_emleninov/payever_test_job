app.module 'HeaderApp.List', (List, app, Backbone, Marionette, $, _) ->
    List.Controller =
        listHeader: ->
            links = app.request('header:entities')
            headers = new (List.Headers)(collection: links)
            headers.on 'brand:clicked', ->
                app.trigger 'albums:list'
                return
            headers.on 'childview:navigate', (childView, model) ->
                trigger = model.get('navigationTrigger')
                app.trigger trigger
                return
            app.getRegion('headerRegion').show headers
            return
        setActiveHeader: (headerUrl) ->
            links = app.request('header:entities')
            headerToSelect = links.find((header) ->
                header.get('url') is headerUrl
            )
            headerToSelect.select()
            links.trigger 'reset'
            return
    return