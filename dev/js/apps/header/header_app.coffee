app.module 'HeaderApp', (Header, app, Backbone, Marionette, $, _) ->
    API = listHeader: ->
        Header.List.Controller.listHeader()
        return
    app.commands.setHandler 'set:active:header', (name) ->
        app.HeaderApp.List.Controller.setActiveHeader name
        return
    Header.on 'start', ->
        API.listHeader()
        return
    return