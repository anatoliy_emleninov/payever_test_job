app.module 'AlbumsApp.List', (List, app, Backbone, Marionette, $, _) ->
    List.Layout = Marionette.LayoutView.extend(
        template: '#albums-layout'
        regions:
            titleRegion: '#title-region'
            albumsRegion: '#albums-region')
    List.Title = Marionette.ItemView.extend(template: '#albums-title')
    List.albumsListItemView = Marionette.ItemView.extend(
        tagName: 'div'
        className: 'col-md-2 b-gallery-item'
        template: '#AlbumsListItemView'
        events: 'click a.js-show': 'showClicked'
        showClicked: (e) ->
            e.preventDefault()
            @trigger 'album:show', @model
            return
    )
    List.albumsCollectionView = Marionette.CollectionView.extend(
        tagName: 'div'
        className: 'row'
        childView: List.albumsListItemView)
    return