app.module 'AlbumsApp.List', (List, app, Backbone, Marionette, $, _) ->
    List.Controller = listAlbums: ->
        albums = app.request('albums:entities')
        albumsLayout = new (List.Layout)
        albumsTitle = new (List.Title)
        albumsListView = new (List.albumsCollectionView)(collection: albums)
        albumsListView.on 'childview:album:show', (childView, model) ->
            app.trigger 'album:show', model.get('id')
            return
        albumsLayout.on 'show', ->
            albumsLayout.titleRegion.show albumsTitle
            albumsLayout.albumsRegion.show albumsListView
            return
        app.getRegion('app').show albumsLayout
        return
    return