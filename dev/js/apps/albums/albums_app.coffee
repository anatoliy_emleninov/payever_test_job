app.module 'AlbumsApp', (AlbumsApp, app, Backbone, Marionette, $, _) ->
    AlbumsApp.Router = Marionette.AppRouter.extend(appRoutes:
        'albums': 'listAlbums'
        'album/:id': 'showAlbum'
        'album/:id/page/:page': 'showAlbum')
    API =
        listAlbums: ->
            AlbumsApp.List.Controller.listAlbums()
            app.execute 'set:active:header', 'albums'
            return
        showAlbum: (id, page) ->
            AlbumsApp.Show.Controller.showAlbumItem id, page
            app.execute 'set:active:header', 'albums'
            return
    app.on 'albums:list', ->
        app.navigate 'albums'
        API.listAlbums()
        return
    app.on 'album:show', (id) ->
        app.navigate 'album/' + id
        API.showAlbum id
        return
    app.on 'album:show:page', (id, page) ->
        console.log 'Subscribe to event and navigate', id, page
        app.navigate 'album/' + id + '/page/' + page
        API.showAlbum id, page
        return
    app.on 'before:start', ->
        new (AlbumsApp.Router)(controller: API)
        return
    return