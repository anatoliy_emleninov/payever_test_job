app.module 'AlbumsApp.Show', (Show, app, Backbone, Marionette, $, _) ->
    Show.albums404 = Marionette.ItemView.extend(template: '#albums-404')
    Show.Layout = Marionette.LayoutView.extend(
        template: '#album-layout'
        regions:
            titleRegion: '#album-title-region'
            albumListRegion: '#album-list-region'
            albumPaginationRegion: '#album-pagination')
    Show.PaginationItem = Marionette.ItemView.extend(
        template: '#AlbumItemView_Pagination_Item'
        tagName: 'li'
        events: 'click li a': 'goToPage'
        goToPage: (e) ->
            e.preventDefault()
            url = $(e.target).attr('href')
            pattern = new UrlPattern('/album(/:id)/page(/:page)')
            query = pattern.match(url)
            @trigger 'album:show:page', query.id, query.page
            @model.getPage parseInt(query.page)
            return
    )
    Show.Pagination = Marionette.CompositeView.extend(
        template: '#AlbumItemView_Pagination'
        tagName: 'nav'
        childView: Show.PaginationItem
        childViewContainer: 'ul'
        onRender: ->
            activeItemIdx = @model.state.currentPage - 1
            @$el.find('li:eq(' + activeItemIdx + ')').addClass 'active'
            return
    )
    Show.Title = Marionette.ItemView.extend(template: '#album-title')
    Show.albumItemView = Marionette.ItemView.extend(
        template: '#AlbumItemView'
        className: 'col-md-3 b-almub-item')
    Show.albumCollectionView = Marionette.CollectionView.extend(
        tagName: 'div'
        className: 'row'
        childView: Show.albumItemView)
    return