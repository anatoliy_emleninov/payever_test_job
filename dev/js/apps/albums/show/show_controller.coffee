app.module 'AlbumsApp.Show', (Show, app, Backbone, Marionette, $, _) ->
    Show.Controller = showAlbumItem: (id, page) ->
        albums = app.request('albums:entities')
        model = albums.get(id)
        Show.AlbumListModel = Backbone.Model.extend({})
        Title = Backbone.Model.extend({})
        titleModel = new Title('title': model.attributes.title)
        albumLayout = new (Show.Layout)
        albumTitle = new (Show.Title)(model: titleModel)
        Show.AlbumListCollection = Backbone.PageableCollection.extend(
            url: 'album/:id/page/:page'
            model: Show.AlbumListModel
            mode: 'client'
            state:
                firstPage: 1
                currentPage: parseInt(page) or 1
                pageSize: 10
            queryParams:
                currentPage: 'current_page'
                pageSize: 'page_size')

        AlbumListModel = for src in model.attributes.images
            src: src

        albumList = new (Show.AlbumListCollection)(AlbumListModel)
        albumListView = new (Show.albumCollectionView)(collection: albumList)
        # Pagination collection
        Show.PaginationModel = Backbone.Model.extend({})
        Show.PaginationCollection = Backbone.Collection.extend(model: Show.PaginationModel)

        PaginationCollectionObj = () ->
            arr = []
            i = 0
            while i < albumList.state.totalPages
                arr[i] =
                    model_id: id
                    page: i + 1
                i++
            arr

        paginationList = new (Show.PaginationCollection)(PaginationCollectionObj(id))
        albumPagination = new (Show.Pagination)(
            model: albumList
            collection: paginationList)
        albumPagination.on 'childview:album:show:page', (childView, id, page) ->
            app.trigger 'album:show:page', id, page
            return

        albumLayout.on 'show', ->
            albumLayout.titleRegion.show albumTitle
            albumLayout.albumListRegion.show albumListView
            if albumList.state.totalPages isnt 1
                albumLayout.albumPaginationRegion.show albumPagination
            return
        app.getRegion('app').show albumLayout
        return
    return