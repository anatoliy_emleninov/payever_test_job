(function() {
  var app;

  console.log('Init');

  app = new Mn.Application;

  app.addRegions({
    headerRegion: '#header-region',
    app: '#app'
  });

  app.module('Entities', function(Entities, app, Backbone, Marionette, $, _) {
    var StorageMixin, findStorageKey;
    findStorageKey = function(entity) {
      if (entity.urlRoot) {
        return _.result(entity, 'urlRoot');
      }
      if (entity.url) {
        return _.result(entity, 'url');
      }
      if (entity.collection && entity.collection.url) {
        return _.result(entity.collection, 'url');
      }
      throw new Error('Unable to determine storage key');
    };
    StorageMixin = function(EntityPrototype) {
      var storageKey;
      storageKey = findStorageKey(EntityPrototype);
      return {
        localStorage: new Backbone.LocalStorage(storageKey)
      };
    };
    Entities.configureStorage = function(entity) {
      _.extend(entity.prototype, new StorageMixin(entity.prototype));
    };
  });

  app.module('Entities', function(Entities, app, Backbone, Marionette, $, _) {
    var API, albums, initAlbums;
    Entities.AlbumModel = Backbone.Model.extend({
      urlRoot: 'albums'
    });
    Entities.configureStorage(Entities.AlbumModel);
    Entities.AlbumsCollection = Backbone.Collection.extend({
      url: 'albums',
      model: Entities.AlbumModel
    });
    Entities.configureStorage(Entities.AlbumsCollection);
    albums = void 0;
    initAlbums = function() {
      albums = new Entities.AlbumsCollection([
        {
          id: 1,
          images: ['public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg'],
          src_thumb: 'public/images/marionette_3.jpg',
          title: 'Title 1'
        }, {
          id: 2,
          images: ['public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg'],
          src_thumb: 'public/images/marionette.jpg',
          title: 'Title 2'
        }, {
          id: 3,
          images: ['public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg'],
          src_thumb: 'public/images/marionette.jpg',
          title: 'Title 3'
        }, {
          id: 4,
          images: ['public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg'],
          src_thumb: 'public/images/marionette.jpg',
          title: 'Title 4'
        }, {
          id: 5,
          images: ['public/images/marionette_3.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg'],
          src_thumb: 'public/images/marionette.jpg',
          title: 'Title 5'
        }, {
          id: 6,
          images: ['public/images/marionette_3.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg', 'public/images/marionette.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette_3.jpg', 'public/images/marionette_2.jpg', 'public/images/marionette.jpg'],
          src_thumb: 'public/images/marionette.jpg',
          title: 'Title 6'
        }
      ]);
      albums.forEach(function(item) {
        item.save();
      });
      return albums;
    };
    API = {
      getAlbumsEntities: function() {
        var getAlbums;
        getAlbums = new Entities.AlbumsCollection;
        getAlbums.fetch();
        if (getAlbums.length === 0) {
          return initAlbums();
        }
        return getAlbums;
      }
    };
    app.reqres.setHandler('albums:entities', function() {
      return API.getAlbumsEntities();
    });
  });

  app.module('Entities', function(Entities, app, Backbone, Marionette, $, _) {
    var API, initHeaders;
    Entities.Header = Backbone.Model.extend({
      initialize: function() {
        Backbone.Select.Me.applyTo(this);
      }
    });
    Entities.HeaderCollection = Backbone.Collection.extend({
      model: Entities.Header,
      initialize: function(models, options) {
        Backbone.Select.One.applyTo(this, models, options);
      }
    });
    initHeaders = function() {
      Entities.headers = new Entities.HeaderCollection([
        {
          name: 'Albums',
          url: 'albums',
          navigationTrigger: 'albums:list'
        }
      ]);
    };
    API = {
      getHeaders: function() {
        if (Entities.headers === void 0) {
          initHeaders();
        }
        return Entities.headers;
      }
    };
    app.reqres.setHandler('header:entities', function() {
      return API.getHeaders();
    });
  });

  app.module('AlbumsApp', function(AlbumsApp, app, Backbone, Marionette, $, _) {
    var API;
    AlbumsApp.Router = Marionette.AppRouter.extend({
      appRoutes: {
        'albums': 'listAlbums',
        'album/:id': 'showAlbum',
        'album/:id/page/:page': 'showAlbum'
      }
    });
    API = {
      listAlbums: function() {
        AlbumsApp.List.Controller.listAlbums();
        app.execute('set:active:header', 'albums');
      },
      showAlbum: function(id, page) {
        AlbumsApp.Show.Controller.showAlbumItem(id, page);
        app.execute('set:active:header', 'albums');
      }
    };
    app.on('albums:list', function() {
      app.navigate('albums');
      API.listAlbums();
    });
    app.on('album:show', function(id) {
      app.navigate('album/' + id);
      API.showAlbum(id);
    });
    app.on('album:show:page', function(id, page) {
      console.log('Subscribe to event and navigate', id, page);
      app.navigate('album/' + id + '/page/' + page);
      API.showAlbum(id, page);
    });
    app.on('before:start', function() {
      new AlbumsApp.Router({
        controller: API
      });
    });
  });

  app.module('HeaderApp', function(Header, app, Backbone, Marionette, $, _) {
    var API;
    API = {
      listHeader: function() {
        Header.List.Controller.listHeader();
      }
    };
    app.commands.setHandler('set:active:header', function(name) {
      app.HeaderApp.List.Controller.setActiveHeader(name);
    });
    Header.on('start', function() {
      API.listHeader();
    });
  });

  app.module('AlbumsApp.List', function(List, app, Backbone, Marionette, $, _) {
    List.Controller = {
      listAlbums: function() {
        var albums, albumsLayout, albumsListView, albumsTitle;
        albums = app.request('albums:entities');
        albumsLayout = new List.Layout;
        albumsTitle = new List.Title;
        albumsListView = new List.albumsCollectionView({
          collection: albums
        });
        albumsListView.on('childview:album:show', function(childView, model) {
          app.trigger('album:show', model.get('id'));
        });
        albumsLayout.on('show', function() {
          albumsLayout.titleRegion.show(albumsTitle);
          albumsLayout.albumsRegion.show(albumsListView);
        });
        app.getRegion('app').show(albumsLayout);
      }
    };
  });

  app.module('AlbumsApp.List', function(List, app, Backbone, Marionette, $, _) {
    List.Layout = Marionette.LayoutView.extend({
      template: '#albums-layout',
      regions: {
        titleRegion: '#title-region',
        albumsRegion: '#albums-region'
      }
    });
    List.Title = Marionette.ItemView.extend({
      template: '#albums-title'
    });
    List.albumsListItemView = Marionette.ItemView.extend({
      tagName: 'div',
      className: 'col-md-2 b-gallery-item',
      template: '#AlbumsListItemView',
      events: {
        'click a.js-show': 'showClicked'
      },
      showClicked: function(e) {
        e.preventDefault();
        this.trigger('album:show', this.model);
      }
    });
    List.albumsCollectionView = Marionette.CollectionView.extend({
      tagName: 'div',
      className: 'row',
      childView: List.albumsListItemView
    });
  });

  app.module('AlbumsApp.Show', function(Show, app, Backbone, Marionette, $, _) {
    Show.Controller = {
      showAlbumItem: function(id, page) {
        var AlbumListModel, PaginationCollectionObj, Title, albumLayout, albumList, albumListView, albumPagination, albumTitle, albums, model, paginationList, src, titleModel;
        albums = app.request('albums:entities');
        model = albums.get(id);
        Show.AlbumListModel = Backbone.Model.extend({});
        Title = Backbone.Model.extend({});
        titleModel = new Title({
          'title': model.attributes.title
        });
        albumLayout = new Show.Layout;
        albumTitle = new Show.Title({
          model: titleModel
        });
        Show.AlbumListCollection = Backbone.PageableCollection.extend({
          url: 'album/:id/page/:page',
          model: Show.AlbumListModel,
          mode: 'client',
          state: {
            firstPage: 1,
            currentPage: parseInt(page) || 1,
            pageSize: 10
          },
          queryParams: {
            currentPage: 'current_page',
            pageSize: 'page_size'
          }
        });
        AlbumListModel = (function() {
          var j, len, ref, results;
          ref = model.attributes.images;
          results = [];
          for (j = 0, len = ref.length; j < len; j++) {
            src = ref[j];
            results.push({
              src: src
            });
          }
          return results;
        })();
        albumList = new Show.AlbumListCollection(AlbumListModel);
        albumListView = new Show.albumCollectionView({
          collection: albumList
        });
        Show.PaginationModel = Backbone.Model.extend({});
        Show.PaginationCollection = Backbone.Collection.extend({
          model: Show.PaginationModel
        });
        PaginationCollectionObj = function() {
          var arr, i;
          arr = [];
          i = 0;
          while (i < albumList.state.totalPages) {
            arr[i] = {
              model_id: id,
              page: i + 1
            };
            i++;
          }
          return arr;
        };
        paginationList = new Show.PaginationCollection(PaginationCollectionObj(id));
        albumPagination = new Show.Pagination({
          model: albumList,
          collection: paginationList
        });
        albumPagination.on('childview:album:show:page', function(childView, id, page) {
          app.trigger('album:show:page', id, page);
        });
        albumLayout.on('show', function() {
          albumLayout.titleRegion.show(albumTitle);
          albumLayout.albumListRegion.show(albumListView);
          if (albumList.state.totalPages !== 1) {
            albumLayout.albumPaginationRegion.show(albumPagination);
          }
        });
        app.getRegion('app').show(albumLayout);
      }
    };
  });

  app.module('AlbumsApp.Show', function(Show, app, Backbone, Marionette, $, _) {
    Show.albums404 = Marionette.ItemView.extend({
      template: '#albums-404'
    });
    Show.Layout = Marionette.LayoutView.extend({
      template: '#album-layout',
      regions: {
        titleRegion: '#album-title-region',
        albumListRegion: '#album-list-region',
        albumPaginationRegion: '#album-pagination'
      }
    });
    Show.PaginationItem = Marionette.ItemView.extend({
      template: '#AlbumItemView_Pagination_Item',
      tagName: 'li',
      events: {
        'click li a': 'goToPage'
      },
      goToPage: function(e) {
        var pattern, query, url;
        e.preventDefault();
        url = $(e.target).attr('href');
        pattern = new UrlPattern('/album(/:id)/page(/:page)');
        query = pattern.match(url);
        this.trigger('album:show:page', query.id, query.page);
        this.model.getPage(parseInt(query.page));
      }
    });
    Show.Pagination = Marionette.CompositeView.extend({
      template: '#AlbumItemView_Pagination',
      tagName: 'nav',
      childView: Show.PaginationItem,
      childViewContainer: 'ul',
      onRender: function() {
        var activeItemIdx;
        activeItemIdx = this.model.state.currentPage - 1;
        this.$el.find('li:eq(' + activeItemIdx + ')').addClass('active');
      }
    });
    Show.Title = Marionette.ItemView.extend({
      template: '#album-title'
    });
    Show.albumItemView = Marionette.ItemView.extend({
      template: '#AlbumItemView',
      className: 'col-md-3 b-almub-item'
    });
    Show.albumCollectionView = Marionette.CollectionView.extend({
      tagName: 'div',
      className: 'row',
      childView: Show.albumItemView
    });
  });

  app.module('HeaderApp.List', function(List, app, Backbone, Marionette, $, _) {
    List.Controller = {
      listHeader: function() {
        var headers, links;
        links = app.request('header:entities');
        headers = new List.Headers({
          collection: links
        });
        headers.on('brand:clicked', function() {
          app.trigger('albums:list');
        });
        headers.on('childview:navigate', function(childView, model) {
          var trigger;
          trigger = model.get('navigationTrigger');
          app.trigger(trigger);
        });
        app.getRegion('headerRegion').show(headers);
      },
      setActiveHeader: function(headerUrl) {
        var headerToSelect, links;
        links = app.request('header:entities');
        headerToSelect = links.find(function(header) {
          return header.get('url') === headerUrl;
        });
        headerToSelect.select();
        links.trigger('reset');
      }
    };
  });

  app.module('HeaderApp.List', function(List, app, Backbone, Marionette, $, _) {
    List.Header = Marionette.ItemView.extend({
      template: '#header-link',
      tagName: 'li',
      events: {
        'click a': 'navigate'
      },
      navigate: function(e) {
        e.preventDefault();
        this.trigger('navigate', this.model);
      },
      onRender: function() {
        if (this.model.selected) {
          this.$el.addClass('active');
        }
      }
    });
    List.Headers = Marionette.CompositeView.extend({
      template: '#header-template',
      tagName: 'nav',
      className: 'navbar navbar-default',
      childView: List.Header,
      childViewContainer: 'ul',
      events: {
        'click a.navbar-brand': 'brandClicked'
      },
      brandClicked: function(e) {
        e.preventDefault();
        this.trigger('brand:clicked');
      }
    });
  });

  console.log('Start');

  app.navigate = function(route, options) {
    if (options == null) {
      options = {};
    }
    Backbone.history.navigate(route, options);
  };

  app.getCurrentRoute = function() {
    return Backbone.history.fragment;
  };

  app.on('start', function() {
    if (Backbone.history) {
      Backbone.history.start();
      if (this.getCurrentRoute() === '') {
        app.trigger('albums:list');
      }
    }
  });

  app.start();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFaOztFQUNBLEdBQUEsR0FBTSxJQUFLLEVBQUUsQ0FBQzs7RUFDZCxHQUFHLENBQUMsVUFBSixDQUNJO0lBQUEsWUFBQSxFQUFjLGdCQUFkO0lBQ0EsR0FBQSxFQUFLLE1BREw7R0FESjs7RUFHQSxHQUFHLENBQUMsTUFBSixDQUFXLFVBQVgsRUFBdUIsU0FBQyxRQUFELEVBQVcsR0FBWCxFQUFnQixRQUFoQixFQUEwQixVQUExQixFQUFzQyxDQUF0QyxFQUF5QyxDQUF6QztBQUVuQixRQUFBO0lBQUEsY0FBQSxHQUFpQixTQUFDLE1BQUQ7TUFDYixJQUFHLE1BQU0sQ0FBQyxPQUFWO0FBQ0ksZUFBTyxDQUFDLENBQUMsTUFBRixDQUFTLE1BQVQsRUFBaUIsU0FBakIsRUFEWDs7TUFFQSxJQUFHLE1BQU0sQ0FBQyxHQUFWO0FBQ0ksZUFBTyxDQUFDLENBQUMsTUFBRixDQUFTLE1BQVQsRUFBaUIsS0FBakIsRUFEWDs7TUFFQSxJQUFHLE1BQU0sQ0FBQyxVQUFQLElBQXNCLE1BQU0sQ0FBQyxVQUFVLENBQUMsR0FBM0M7QUFDSSxlQUFPLENBQUMsQ0FBQyxNQUFGLENBQVMsTUFBTSxDQUFDLFVBQWhCLEVBQTRCLEtBQTVCLEVBRFg7O0FBRUEsWUFBVSxJQUFBLEtBQUEsQ0FBTSxpQ0FBTjtJQVBHO0lBVWpCLFlBQUEsR0FBZSxTQUFDLGVBQUQ7QUFDWCxVQUFBO01BQUEsVUFBQSxHQUFhLGNBQUEsQ0FBZSxlQUFmO2FBQ2I7UUFBRSxZQUFBLEVBQWtCLElBQUMsUUFBUSxDQUFDLFlBQVYsQ0FBd0IsVUFBeEIsQ0FBcEI7O0lBRlc7SUFJZixRQUFRLENBQUMsZ0JBQVQsR0FBNEIsU0FBQyxNQUFEO01BQ3hCLENBQUMsQ0FBQyxNQUFGLENBQVMsTUFBTSxDQUFDLFNBQWhCLEVBQStCLElBQUEsWUFBQSxDQUFhLE1BQU0sQ0FBQyxTQUFwQixDQUEvQjtJQUR3QjtFQWhCVCxDQUF2Qjs7RUFxQkEsR0FBRyxDQUFDLE1BQUosQ0FBVyxVQUFYLEVBQXVCLFNBQUMsUUFBRCxFQUFXLEdBQVgsRUFBZ0IsUUFBaEIsRUFBMEIsVUFBMUIsRUFBc0MsQ0FBdEMsRUFBeUMsQ0FBekM7QUFDbkIsUUFBQTtJQUFBLFFBQVEsQ0FBQyxVQUFULEdBQXNCLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBZixDQUFzQjtNQUFBLE9BQUEsRUFBUyxRQUFUO0tBQXRCO0lBQ3RCLFFBQVEsQ0FBQyxnQkFBVCxDQUEwQixRQUFRLENBQUMsVUFBbkM7SUFDQSxRQUFRLENBQUMsZ0JBQVQsR0FBNEIsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFwQixDQUN4QjtNQUFBLEdBQUEsRUFBSyxRQUFMO01BQ0EsS0FBQSxFQUFPLFFBQVEsQ0FBQyxVQURoQjtLQUR3QjtJQUk1QixRQUFRLENBQUMsZ0JBQVQsQ0FBMEIsUUFBUSxDQUFDLGdCQUFuQztJQUNBLE1BQUEsR0FBUztJQUVULFVBQUEsR0FBYSxTQUFBO01BQ1QsTUFBQSxHQUFhLElBQUMsUUFBUSxDQUFDLGdCQUFWLENBQTRCO1FBQ3JDO1VBQ0ksRUFBQSxFQUFJLENBRFI7VUFFSSxNQUFBLEVBQVEsQ0FDSixnQ0FESSxFQUVKLGdDQUZJLEVBR0osZ0NBSEksRUFJSixnQ0FKSSxFQUtKLDhCQUxJLENBRlo7VUFTSSxTQUFBLEVBQVcsZ0NBVGY7VUFVSSxLQUFBLEVBQU8sU0FWWDtTQURxQyxFQWFyQztVQUNJLEVBQUEsRUFBSSxDQURSO1VBRUksTUFBQSxFQUFRLENBQ0osOEJBREksRUFFSixnQ0FGSSxFQUdKLGdDQUhJLEVBSUosZ0NBSkksRUFLSixnQ0FMSSxFQU1KLGdDQU5JLEVBT0osZ0NBUEksRUFRSixnQ0FSSSxFQVNKLGdDQVRJLEVBVUosOEJBVkksRUFXSixnQ0FYSSxFQVlKLGdDQVpJLEVBYUosZ0NBYkksRUFjSixnQ0FkSSxFQWVKLDhCQWZJLEVBZ0JKLGdDQWhCSSxFQWlCSixnQ0FqQkksRUFrQkosZ0NBbEJJLEVBbUJKLGdDQW5CSSxFQW9CSiw4QkFwQkksRUFxQkosZ0NBckJJLEVBc0JKLGdDQXRCSSxFQXVCSixnQ0F2QkksRUF3QkosZ0NBeEJJLEVBeUJKLDhCQXpCSSxDQUZaO1VBNkJJLFNBQUEsRUFBVyw4QkE3QmY7VUE4QkksS0FBQSxFQUFPLFNBOUJYO1NBYnFDLEVBNkNyQztVQUNJLEVBQUEsRUFBSSxDQURSO1VBRUksTUFBQSxFQUFRLENBQ0osOEJBREksRUFFSixnQ0FGSSxFQUdKLGdDQUhJLEVBSUosZ0NBSkksRUFLSixnQ0FMSSxFQU1KLGdDQU5JLEVBT0osZ0NBUEksRUFRSixnQ0FSSSxFQVNKLGdDQVRJLEVBVUosOEJBVkksRUFXSixnQ0FYSSxFQVlKLGdDQVpJLEVBYUosZ0NBYkksRUFjSixnQ0FkSSxFQWVKLDhCQWZJLEVBZ0JKLGdDQWhCSSxFQWlCSixnQ0FqQkksRUFrQkosZ0NBbEJJLEVBbUJKLGdDQW5CSSxFQW9CSiw4QkFwQkksRUFxQkosZ0NBckJJLEVBc0JKLGdDQXRCSSxFQXVCSixnQ0F2QkksRUF3QkosZ0NBeEJJLEVBeUJKLDhCQXpCSSxFQTBCSixnQ0ExQkksRUEyQkosZ0NBM0JJLEVBNEJKLGdDQTVCSSxFQTZCSiw4QkE3QkksQ0FGWjtVQWlDSSxTQUFBLEVBQVcsOEJBakNmO1VBa0NJLEtBQUEsRUFBTyxTQWxDWDtTQTdDcUMsRUFpRnJDO1VBQ0ksRUFBQSxFQUFJLENBRFI7VUFFSSxNQUFBLEVBQVEsQ0FDSiw4QkFESSxFQUVKLGdDQUZJLEVBR0osZ0NBSEksRUFJSixnQ0FKSSxFQUtKLGdDQUxJLEVBTUosZ0NBTkksRUFPSixnQ0FQSSxFQVFKLDhCQVJJLEVBU0osZ0NBVEksRUFVSixnQ0FWSSxFQVdKLGdDQVhJLEVBWUosOEJBWkksRUFhSixnQ0FiSSxFQWNKLGdDQWRJLEVBZUosZ0NBZkksRUFnQkosOEJBaEJJLEVBaUJKLGdDQWpCSSxFQWtCSixnQ0FsQkksRUFtQkosZ0NBbkJJLEVBb0JKLDhCQXBCSSxFQXFCSixnQ0FyQkksRUFzQkosZ0NBdEJJLEVBdUJKLGdDQXZCSSxFQXdCSiw4QkF4QkksQ0FGWjtVQTRCSSxTQUFBLEVBQVcsOEJBNUJmO1VBNkJJLEtBQUEsRUFBTyxTQTdCWDtTQWpGcUMsRUFnSHJDO1VBQ0ksRUFBQSxFQUFJLENBRFI7VUFFSSxNQUFBLEVBQVEsQ0FDSixnQ0FESSxFQUVKLGdDQUZJLEVBR0osOEJBSEksRUFJSixnQ0FKSSxFQUtKLGdDQUxJLEVBTUosZ0NBTkksRUFPSixnQ0FQSSxFQVFKLDhCQVJJLEVBU0osZ0NBVEksRUFVSixnQ0FWSSxFQVdKLGdDQVhJLEVBWUosOEJBWkksRUFhSixnQ0FiSSxFQWNKLGdDQWRJLEVBZUosZ0NBZkksRUFnQkosOEJBaEJJLEVBaUJKLGdDQWpCSSxFQWtCSixnQ0FsQkksRUFtQkosZ0NBbkJJLEVBb0JKLDhCQXBCSSxFQXFCSixnQ0FyQkksRUFzQkosZ0NBdEJJLENBRlo7VUEwQkksU0FBQSxFQUFXLDhCQTFCZjtVQTJCSSxLQUFBLEVBQU8sU0EzQlg7U0FoSHFDLEVBNklyQztVQUNJLEVBQUEsRUFBSSxDQURSO1VBRUksTUFBQSxFQUFRLENBQ0osZ0NBREksRUFFSixnQ0FGSSxFQUdKLGdDQUhJLEVBSUosOEJBSkksRUFLSixnQ0FMSSxFQU1KLGdDQU5JLEVBT0osZ0NBUEksRUFRSiw4QkFSSSxFQVNKLGdDQVRJLEVBVUosZ0NBVkksRUFXSixnQ0FYSSxFQVlKLDhCQVpJLEVBYUosZ0NBYkksRUFjSixnQ0FkSSxFQWVKLGdDQWZJLEVBZ0JKLDhCQWhCSSxFQWlCSixnQ0FqQkksRUFrQkosZ0NBbEJJLEVBbUJKLGdDQW5CSSxFQW9CSiw4QkFwQkksRUFxQkosZ0NBckJJLEVBc0JKLGdDQXRCSSxFQXVCSixnQ0F2QkksRUF3QkosOEJBeEJJLEVBeUJKLDhCQXpCSSxFQTBCSixnQ0ExQkksRUEyQkosZ0NBM0JJLEVBNEJKLGdDQTVCSSxFQTZCSiw4QkE3QkksQ0FGWjtVQWlDSSxTQUFBLEVBQVcsOEJBakNmO1VBa0NJLEtBQUEsRUFBTyxTQWxDWDtTQTdJcUM7T0FBNUI7TUFrTGIsTUFBTSxDQUFDLE9BQVAsQ0FBZSxTQUFDLElBQUQ7UUFDWCxJQUFJLENBQUMsSUFBTCxDQUFBO01BRFcsQ0FBZjthQUdBO0lBdExTO0lBd0xiLEdBQUEsR0FBTTtNQUFBLGlCQUFBLEVBQW1CLFNBQUE7QUFDckIsWUFBQTtRQUFBLFNBQUEsR0FBWSxJQUFLLFFBQVEsQ0FBQztRQUMxQixTQUFTLENBQUMsS0FBVixDQUFBO1FBQ0EsSUFBRyxTQUFTLENBQUMsTUFBVixLQUFvQixDQUF2QjtBQUNJLGlCQUFPLFVBQUEsQ0FBQSxFQURYOztlQUVBO01BTHFCLENBQW5COztJQU1OLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBWCxDQUFzQixpQkFBdEIsRUFBeUMsU0FBQTthQUNyQyxHQUFHLENBQUMsaUJBQUosQ0FBQTtJQURxQyxDQUF6QztFQXhNbUIsQ0FBdkI7O0VBMk1BLEdBQUcsQ0FBQyxNQUFKLENBQVcsVUFBWCxFQUF1QixTQUFDLFFBQUQsRUFBVyxHQUFYLEVBQWdCLFFBQWhCLEVBQTBCLFVBQTFCLEVBQXNDLENBQXRDLEVBQXlDLENBQXpDO0FBQ25CLFFBQUE7SUFBQSxRQUFRLENBQUMsTUFBVCxHQUFrQixRQUFRLENBQUMsS0FBSyxDQUFDLE1BQWYsQ0FBc0I7TUFBQSxVQUFBLEVBQVksU0FBQTtRQUNoRCxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFuQixDQUEyQixJQUEzQjtNQURnRCxDQUFaO0tBQXRCO0lBSWxCLFFBQVEsQ0FBQyxnQkFBVCxHQUE0QixRQUFRLENBQUMsVUFBVSxDQUFDLE1BQXBCLENBQ3hCO01BQUEsS0FBQSxFQUFPLFFBQVEsQ0FBQyxNQUFoQjtNQUNBLFVBQUEsRUFBWSxTQUFDLE1BQUQsRUFBUyxPQUFUO1FBQ1IsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBcEIsQ0FBNEIsSUFBNUIsRUFBa0MsTUFBbEMsRUFBMEMsT0FBMUM7TUFEUSxDQURaO0tBRHdCO0lBTzVCLFdBQUEsR0FBYyxTQUFBO01BQ1YsUUFBUSxDQUFDLE9BQVQsR0FBdUIsSUFBQyxRQUFRLENBQUMsZ0JBQVYsQ0FBNEI7UUFBRTtVQUNqRCxJQUFBLEVBQU0sUUFEMkM7VUFFakQsR0FBQSxFQUFLLFFBRjRDO1VBR2pELGlCQUFBLEVBQW1CLGFBSDhCO1NBQUY7T0FBNUI7SUFEYjtJQVFkLEdBQUEsR0FBTTtNQUFBLFVBQUEsRUFBWSxTQUFBO1FBQ2QsSUFBRyxRQUFRLENBQUMsT0FBVCxLQUFvQixNQUF2QjtVQUNJLFdBQUEsQ0FBQSxFQURKOztlQUVBLFFBQVEsQ0FBQztNQUhLLENBQVo7O0lBSU4sR0FBRyxDQUFDLE1BQU0sQ0FBQyxVQUFYLENBQXNCLGlCQUF0QixFQUF5QyxTQUFBO2FBQ3JDLEdBQUcsQ0FBQyxVQUFKLENBQUE7SUFEcUMsQ0FBekM7RUF4Qm1CLENBQXZCOztFQTJCQSxHQUFHLENBQUMsTUFBSixDQUFXLFdBQVgsRUFBd0IsU0FBQyxTQUFELEVBQVksR0FBWixFQUFpQixRQUFqQixFQUEyQixVQUEzQixFQUF1QyxDQUF2QyxFQUEwQyxDQUExQztBQUNwQixRQUFBO0lBQUEsU0FBUyxDQUFDLE1BQVYsR0FBbUIsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFyQixDQUE0QjtNQUFBLFNBQUEsRUFDM0M7UUFBQSxRQUFBLEVBQVUsWUFBVjtRQUNBLFdBQUEsRUFBYSxXQURiO1FBRUEsc0JBQUEsRUFBd0IsV0FGeEI7T0FEMkM7S0FBNUI7SUFJbkIsR0FBQSxHQUNJO01BQUEsVUFBQSxFQUFZLFNBQUE7UUFDUixTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUExQixDQUFBO1FBQ0EsR0FBRyxDQUFDLE9BQUosQ0FBWSxtQkFBWixFQUFpQyxRQUFqQztNQUZRLENBQVo7TUFJQSxTQUFBLEVBQVcsU0FBQyxFQUFELEVBQUssSUFBTDtRQUNQLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQTFCLENBQXdDLEVBQXhDLEVBQTRDLElBQTVDO1FBQ0EsR0FBRyxDQUFDLE9BQUosQ0FBWSxtQkFBWixFQUFpQyxRQUFqQztNQUZPLENBSlg7O0lBUUosR0FBRyxDQUFDLEVBQUosQ0FBTyxhQUFQLEVBQXNCLFNBQUE7TUFDbEIsR0FBRyxDQUFDLFFBQUosQ0FBYSxRQUFiO01BQ0EsR0FBRyxDQUFDLFVBQUosQ0FBQTtJQUZrQixDQUF0QjtJQUlBLEdBQUcsQ0FBQyxFQUFKLENBQU8sWUFBUCxFQUFxQixTQUFDLEVBQUQ7TUFDakIsR0FBRyxDQUFDLFFBQUosQ0FBYSxRQUFBLEdBQVcsRUFBeEI7TUFDQSxHQUFHLENBQUMsU0FBSixDQUFjLEVBQWQ7SUFGaUIsQ0FBckI7SUFJQSxHQUFHLENBQUMsRUFBSixDQUFPLGlCQUFQLEVBQTBCLFNBQUMsRUFBRCxFQUFLLElBQUw7TUFDdEIsT0FBTyxDQUFDLEdBQVIsQ0FBWSxpQ0FBWixFQUErQyxFQUEvQyxFQUFtRCxJQUFuRDtNQUNBLEdBQUcsQ0FBQyxRQUFKLENBQWEsUUFBQSxHQUFXLEVBQVgsR0FBZ0IsUUFBaEIsR0FBMkIsSUFBeEM7TUFDQSxHQUFHLENBQUMsU0FBSixDQUFjLEVBQWQsRUFBa0IsSUFBbEI7SUFIc0IsQ0FBMUI7SUFLQSxHQUFHLENBQUMsRUFBSixDQUFPLGNBQVAsRUFBdUIsU0FBQTtNQUNmLElBQUMsU0FBUyxDQUFDLE1BQVgsQ0FBbUI7UUFBQSxVQUFBLEVBQVksR0FBWjtPQUFuQjtJQURlLENBQXZCO0VBM0JvQixDQUF4Qjs7RUErQkEsR0FBRyxDQUFDLE1BQUosQ0FBVyxXQUFYLEVBQXdCLFNBQUMsTUFBRCxFQUFTLEdBQVQsRUFBYyxRQUFkLEVBQXdCLFVBQXhCLEVBQW9DLENBQXBDLEVBQXVDLENBQXZDO0FBQ3BCLFFBQUE7SUFBQSxHQUFBLEdBQU07TUFBQSxVQUFBLEVBQVksU0FBQTtRQUNkLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQXZCLENBQUE7TUFEYyxDQUFaOztJQUdOLEdBQUcsQ0FBQyxRQUFRLENBQUMsVUFBYixDQUF3QixtQkFBeEIsRUFBNkMsU0FBQyxJQUFEO01BQ3pDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUE5QixDQUE4QyxJQUE5QztJQUR5QyxDQUE3QztJQUdBLE1BQU0sQ0FBQyxFQUFQLENBQVUsT0FBVixFQUFtQixTQUFBO01BQ2YsR0FBRyxDQUFDLFVBQUosQ0FBQTtJQURlLENBQW5CO0VBUG9CLENBQXhCOztFQVdBLEdBQUcsQ0FBQyxNQUFKLENBQVcsZ0JBQVgsRUFBNkIsU0FBQyxJQUFELEVBQU8sR0FBUCxFQUFZLFFBQVosRUFBc0IsVUFBdEIsRUFBa0MsQ0FBbEMsRUFBcUMsQ0FBckM7SUFDekIsSUFBSSxDQUFDLFVBQUwsR0FBa0I7TUFBQSxVQUFBLEVBQVksU0FBQTtBQUMxQixZQUFBO1FBQUEsTUFBQSxHQUFTLEdBQUcsQ0FBQyxPQUFKLENBQVksaUJBQVo7UUFDVCxZQUFBLEdBQWUsSUFBSyxJQUFJLENBQUM7UUFDekIsV0FBQSxHQUFjLElBQUssSUFBSSxDQUFDO1FBQ3hCLGNBQUEsR0FBcUIsSUFBQyxJQUFJLENBQUMsb0JBQU4sQ0FBNEI7VUFBQSxVQUFBLEVBQVksTUFBWjtTQUE1QjtRQUNyQixjQUFjLENBQUMsRUFBZixDQUFrQixzQkFBbEIsRUFBMEMsU0FBQyxTQUFELEVBQVksS0FBWjtVQUN0QyxHQUFHLENBQUMsT0FBSixDQUFZLFlBQVosRUFBMEIsS0FBSyxDQUFDLEdBQU4sQ0FBVSxJQUFWLENBQTFCO1FBRHNDLENBQTFDO1FBR0EsWUFBWSxDQUFDLEVBQWIsQ0FBZ0IsTUFBaEIsRUFBd0IsU0FBQTtVQUNwQixZQUFZLENBQUMsV0FBVyxDQUFDLElBQXpCLENBQThCLFdBQTlCO1VBQ0EsWUFBWSxDQUFDLFlBQVksQ0FBQyxJQUExQixDQUErQixjQUEvQjtRQUZvQixDQUF4QjtRQUlBLEdBQUcsQ0FBQyxTQUFKLENBQWMsS0FBZCxDQUFvQixDQUFDLElBQXJCLENBQTBCLFlBQTFCO01BWjBCLENBQVo7O0VBRE8sQ0FBN0I7O0VBZ0JBLEdBQUcsQ0FBQyxNQUFKLENBQVcsZ0JBQVgsRUFBNkIsU0FBQyxJQUFELEVBQU8sR0FBUCxFQUFZLFFBQVosRUFBc0IsVUFBdEIsRUFBa0MsQ0FBbEMsRUFBcUMsQ0FBckM7SUFDekIsSUFBSSxDQUFDLE1BQUwsR0FBYyxVQUFVLENBQUMsVUFBVSxDQUFDLE1BQXRCLENBQ1Y7TUFBQSxRQUFBLEVBQVUsZ0JBQVY7TUFDQSxPQUFBLEVBQ0k7UUFBQSxXQUFBLEVBQWEsZUFBYjtRQUNBLFlBQUEsRUFBYyxnQkFEZDtPQUZKO0tBRFU7SUFLZCxJQUFJLENBQUMsS0FBTCxHQUFhLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBcEIsQ0FBMkI7TUFBQSxRQUFBLEVBQVUsZUFBVjtLQUEzQjtJQUNiLElBQUksQ0FBQyxrQkFBTCxHQUEwQixVQUFVLENBQUMsUUFBUSxDQUFDLE1BQXBCLENBQ3RCO01BQUEsT0FBQSxFQUFTLEtBQVQ7TUFDQSxTQUFBLEVBQVcseUJBRFg7TUFFQSxRQUFBLEVBQVUscUJBRlY7TUFHQSxNQUFBLEVBQVE7UUFBQSxpQkFBQSxFQUFtQixhQUFuQjtPQUhSO01BSUEsV0FBQSxFQUFhLFNBQUMsQ0FBRDtRQUNULENBQUMsQ0FBQyxjQUFGLENBQUE7UUFDQSxJQUFDLENBQUEsT0FBRCxDQUFTLFlBQVQsRUFBdUIsSUFBQyxDQUFBLEtBQXhCO01BRlMsQ0FKYjtLQURzQjtJQVUxQixJQUFJLENBQUMsb0JBQUwsR0FBNEIsVUFBVSxDQUFDLGNBQWMsQ0FBQyxNQUExQixDQUN4QjtNQUFBLE9BQUEsRUFBUyxLQUFUO01BQ0EsU0FBQSxFQUFXLEtBRFg7TUFFQSxTQUFBLEVBQVcsSUFBSSxDQUFDLGtCQUZoQjtLQUR3QjtFQWpCSCxDQUE3Qjs7RUFzQkEsR0FBRyxDQUFDLE1BQUosQ0FBVyxnQkFBWCxFQUE2QixTQUFDLElBQUQsRUFBTyxHQUFQLEVBQVksUUFBWixFQUFzQixVQUF0QixFQUFrQyxDQUFsQyxFQUFxQyxDQUFyQztJQUN6QixJQUFJLENBQUMsVUFBTCxHQUFrQjtNQUFBLGFBQUEsRUFBZSxTQUFDLEVBQUQsRUFBSyxJQUFMO0FBQzdCLFlBQUE7UUFBQSxNQUFBLEdBQVMsR0FBRyxDQUFDLE9BQUosQ0FBWSxpQkFBWjtRQUNULEtBQUEsR0FBUSxNQUFNLENBQUMsR0FBUCxDQUFXLEVBQVg7UUFDUixJQUFJLENBQUMsY0FBTCxHQUFzQixRQUFRLENBQUMsS0FBSyxDQUFDLE1BQWYsQ0FBc0IsRUFBdEI7UUFDdEIsS0FBQSxHQUFRLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBZixDQUFzQixFQUF0QjtRQUNSLFVBQUEsR0FBaUIsSUFBQSxLQUFBLENBQU07VUFBQSxPQUFBLEVBQVMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUExQjtTQUFOO1FBQ2pCLFdBQUEsR0FBYyxJQUFLLElBQUksQ0FBQztRQUN4QixVQUFBLEdBQWlCLElBQUMsSUFBSSxDQUFDLEtBQU4sQ0FBYTtVQUFBLEtBQUEsRUFBTyxVQUFQO1NBQWI7UUFDakIsSUFBSSxDQUFDLG1CQUFMLEdBQTJCLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxNQUE1QixDQUN2QjtVQUFBLEdBQUEsRUFBSyxzQkFBTDtVQUNBLEtBQUEsRUFBTyxJQUFJLENBQUMsY0FEWjtVQUVBLElBQUEsRUFBTSxRQUZOO1VBR0EsS0FBQSxFQUNJO1lBQUEsU0FBQSxFQUFXLENBQVg7WUFDQSxXQUFBLEVBQWEsUUFBQSxDQUFTLElBQVQsQ0FBQSxJQUFrQixDQUQvQjtZQUVBLFFBQUEsRUFBVSxFQUZWO1dBSko7VUFPQSxXQUFBLEVBQ0k7WUFBQSxXQUFBLEVBQWEsY0FBYjtZQUNBLFFBQUEsRUFBVSxXQURWO1dBUko7U0FEdUI7UUFZM0IsY0FBQTs7QUFBaUI7QUFBQTtlQUFBLHFDQUFBOzt5QkFDYjtjQUFBLEdBQUEsRUFBSyxHQUFMOztBQURhOzs7UUFHakIsU0FBQSxHQUFnQixJQUFDLElBQUksQ0FBQyxtQkFBTixDQUEyQixjQUEzQjtRQUNoQixhQUFBLEdBQW9CLElBQUMsSUFBSSxDQUFDLG1CQUFOLENBQTJCO1VBQUEsVUFBQSxFQUFZLFNBQVo7U0FBM0I7UUFFcEIsSUFBSSxDQUFDLGVBQUwsR0FBdUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFmLENBQXNCLEVBQXRCO1FBQ3ZCLElBQUksQ0FBQyxvQkFBTCxHQUE0QixRQUFRLENBQUMsVUFBVSxDQUFDLE1BQXBCLENBQTJCO1VBQUEsS0FBQSxFQUFPLElBQUksQ0FBQyxlQUFaO1NBQTNCO1FBRTVCLHVCQUFBLEdBQTBCLFNBQUE7QUFDdEIsY0FBQTtVQUFBLEdBQUEsR0FBTTtVQUNOLENBQUEsR0FBSTtBQUNKLGlCQUFNLENBQUEsR0FBSSxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQTFCO1lBQ0ksR0FBSSxDQUFBLENBQUEsQ0FBSixHQUNJO2NBQUEsUUFBQSxFQUFVLEVBQVY7Y0FDQSxJQUFBLEVBQU0sQ0FBQSxHQUFJLENBRFY7O1lBRUosQ0FBQTtVQUpKO2lCQUtBO1FBUnNCO1FBVTFCLGNBQUEsR0FBcUIsSUFBQyxJQUFJLENBQUMsb0JBQU4sQ0FBNEIsdUJBQUEsQ0FBd0IsRUFBeEIsQ0FBNUI7UUFDckIsZUFBQSxHQUFzQixJQUFDLElBQUksQ0FBQyxVQUFOLENBQ2xCO1VBQUEsS0FBQSxFQUFPLFNBQVA7VUFDQSxVQUFBLEVBQVksY0FEWjtTQURrQjtRQUd0QixlQUFlLENBQUMsRUFBaEIsQ0FBbUIsMkJBQW5CLEVBQWdELFNBQUMsU0FBRCxFQUFZLEVBQVosRUFBZ0IsSUFBaEI7VUFDNUMsR0FBRyxDQUFDLE9BQUosQ0FBWSxpQkFBWixFQUErQixFQUEvQixFQUFtQyxJQUFuQztRQUQ0QyxDQUFoRDtRQUlBLFdBQVcsQ0FBQyxFQUFaLENBQWUsTUFBZixFQUF1QixTQUFBO1VBQ25CLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBeEIsQ0FBNkIsVUFBN0I7VUFDQSxXQUFXLENBQUMsZUFBZSxDQUFDLElBQTVCLENBQWlDLGFBQWpDO1VBQ0EsSUFBRyxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQWhCLEtBQWdDLENBQW5DO1lBQ0ksV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQWxDLENBQXVDLGVBQXZDLEVBREo7O1FBSG1CLENBQXZCO1FBTUEsR0FBRyxDQUFDLFNBQUosQ0FBYyxLQUFkLENBQW9CLENBQUMsSUFBckIsQ0FBMEIsV0FBMUI7TUFyRDZCLENBQWY7O0VBRE8sQ0FBN0I7O0VBeURBLEdBQUcsQ0FBQyxNQUFKLENBQVcsZ0JBQVgsRUFBNkIsU0FBQyxJQUFELEVBQU8sR0FBUCxFQUFZLFFBQVosRUFBc0IsVUFBdEIsRUFBa0MsQ0FBbEMsRUFBcUMsQ0FBckM7SUFDekIsSUFBSSxDQUFDLFNBQUwsR0FBaUIsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFwQixDQUEyQjtNQUFBLFFBQUEsRUFBVSxhQUFWO0tBQTNCO0lBQ2pCLElBQUksQ0FBQyxNQUFMLEdBQWMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUF0QixDQUNWO01BQUEsUUFBQSxFQUFVLGVBQVY7TUFDQSxPQUFBLEVBQ0k7UUFBQSxXQUFBLEVBQWEscUJBQWI7UUFDQSxlQUFBLEVBQWlCLG9CQURqQjtRQUVBLHFCQUFBLEVBQXVCLG1CQUZ2QjtPQUZKO0tBRFU7SUFNZCxJQUFJLENBQUMsY0FBTCxHQUFzQixVQUFVLENBQUMsUUFBUSxDQUFDLE1BQXBCLENBQ2xCO01BQUEsUUFBQSxFQUFVLGdDQUFWO01BQ0EsT0FBQSxFQUFTLElBRFQ7TUFFQSxNQUFBLEVBQVE7UUFBQSxZQUFBLEVBQWMsVUFBZDtPQUZSO01BR0EsUUFBQSxFQUFVLFNBQUMsQ0FBRDtBQUNOLFlBQUE7UUFBQSxDQUFDLENBQUMsY0FBRixDQUFBO1FBQ0EsR0FBQSxHQUFNLENBQUEsQ0FBRSxDQUFDLENBQUMsTUFBSixDQUFXLENBQUMsSUFBWixDQUFpQixNQUFqQjtRQUNOLE9BQUEsR0FBYyxJQUFBLFVBQUEsQ0FBVywyQkFBWDtRQUNkLEtBQUEsR0FBUSxPQUFPLENBQUMsS0FBUixDQUFjLEdBQWQ7UUFDUixJQUFDLENBQUEsT0FBRCxDQUFTLGlCQUFULEVBQTRCLEtBQUssQ0FBQyxFQUFsQyxFQUFzQyxLQUFLLENBQUMsSUFBNUM7UUFDQSxJQUFDLENBQUEsS0FBSyxDQUFDLE9BQVAsQ0FBZSxRQUFBLENBQVMsS0FBSyxDQUFDLElBQWYsQ0FBZjtNQU5NLENBSFY7S0FEa0I7SUFhdEIsSUFBSSxDQUFDLFVBQUwsR0FBa0IsVUFBVSxDQUFDLGFBQWEsQ0FBQyxNQUF6QixDQUNkO01BQUEsUUFBQSxFQUFVLDJCQUFWO01BQ0EsT0FBQSxFQUFTLEtBRFQ7TUFFQSxTQUFBLEVBQVcsSUFBSSxDQUFDLGNBRmhCO01BR0Esa0JBQUEsRUFBb0IsSUFIcEI7TUFJQSxRQUFBLEVBQVUsU0FBQTtBQUNOLFlBQUE7UUFBQSxhQUFBLEdBQWdCLElBQUMsQ0FBQSxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQWIsR0FBMkI7UUFDM0MsSUFBQyxDQUFBLEdBQUcsQ0FBQyxJQUFMLENBQVUsUUFBQSxHQUFXLGFBQVgsR0FBMkIsR0FBckMsQ0FBeUMsQ0FBQyxRQUExQyxDQUFtRCxRQUFuRDtNQUZNLENBSlY7S0FEYztJQVVsQixJQUFJLENBQUMsS0FBTCxHQUFhLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBcEIsQ0FBMkI7TUFBQSxRQUFBLEVBQVUsY0FBVjtLQUEzQjtJQUNiLElBQUksQ0FBQyxhQUFMLEdBQXFCLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBcEIsQ0FDakI7TUFBQSxRQUFBLEVBQVUsZ0JBQVY7TUFDQSxTQUFBLEVBQVcsdUJBRFg7S0FEaUI7SUFHckIsSUFBSSxDQUFDLG1CQUFMLEdBQTJCLFVBQVUsQ0FBQyxjQUFjLENBQUMsTUFBMUIsQ0FDdkI7TUFBQSxPQUFBLEVBQVMsS0FBVDtNQUNBLFNBQUEsRUFBVyxLQURYO01BRUEsU0FBQSxFQUFXLElBQUksQ0FBQyxhQUZoQjtLQUR1QjtFQW5DRixDQUE3Qjs7RUF3Q0EsR0FBRyxDQUFDLE1BQUosQ0FBVyxnQkFBWCxFQUE2QixTQUFDLElBQUQsRUFBTyxHQUFQLEVBQVksUUFBWixFQUFzQixVQUF0QixFQUFrQyxDQUFsQyxFQUFxQyxDQUFyQztJQUN6QixJQUFJLENBQUMsVUFBTCxHQUNJO01BQUEsVUFBQSxFQUFZLFNBQUE7QUFDUixZQUFBO1FBQUEsS0FBQSxHQUFRLEdBQUcsQ0FBQyxPQUFKLENBQVksaUJBQVo7UUFDUixPQUFBLEdBQWMsSUFBQyxJQUFJLENBQUMsT0FBTixDQUFlO1VBQUEsVUFBQSxFQUFZLEtBQVo7U0FBZjtRQUNkLE9BQU8sQ0FBQyxFQUFSLENBQVcsZUFBWCxFQUE0QixTQUFBO1VBQ3hCLEdBQUcsQ0FBQyxPQUFKLENBQVksYUFBWjtRQUR3QixDQUE1QjtRQUdBLE9BQU8sQ0FBQyxFQUFSLENBQVcsb0JBQVgsRUFBaUMsU0FBQyxTQUFELEVBQVksS0FBWjtBQUM3QixjQUFBO1VBQUEsT0FBQSxHQUFVLEtBQUssQ0FBQyxHQUFOLENBQVUsbUJBQVY7VUFDVixHQUFHLENBQUMsT0FBSixDQUFZLE9BQVo7UUFGNkIsQ0FBakM7UUFJQSxHQUFHLENBQUMsU0FBSixDQUFjLGNBQWQsQ0FBNkIsQ0FBQyxJQUE5QixDQUFtQyxPQUFuQztNQVZRLENBQVo7TUFZQSxlQUFBLEVBQWlCLFNBQUMsU0FBRDtBQUNiLFlBQUE7UUFBQSxLQUFBLEdBQVEsR0FBRyxDQUFDLE9BQUosQ0FBWSxpQkFBWjtRQUNSLGNBQUEsR0FBaUIsS0FBSyxDQUFDLElBQU4sQ0FBVyxTQUFDLE1BQUQ7aUJBQ3hCLE1BQU0sQ0FBQyxHQUFQLENBQVcsS0FBWCxDQUFBLEtBQXFCO1FBREcsQ0FBWDtRQUdqQixjQUFjLENBQUMsTUFBZixDQUFBO1FBQ0EsS0FBSyxDQUFDLE9BQU4sQ0FBYyxPQUFkO01BTmEsQ0FaakI7O0VBRnFCLENBQTdCOztFQXVCQSxHQUFHLENBQUMsTUFBSixDQUFXLGdCQUFYLEVBQTZCLFNBQUMsSUFBRCxFQUFPLEdBQVAsRUFBWSxRQUFaLEVBQXNCLFVBQXRCLEVBQWtDLENBQWxDLEVBQXFDLENBQXJDO0lBQ3pCLElBQUksQ0FBQyxNQUFMLEdBQWMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFwQixDQUNWO01BQUEsUUFBQSxFQUFVLGNBQVY7TUFDQSxPQUFBLEVBQVMsSUFEVDtNQUVBLE1BQUEsRUFBUTtRQUFBLFNBQUEsRUFBVyxVQUFYO09BRlI7TUFHQSxRQUFBLEVBQVUsU0FBQyxDQUFEO1FBQ04sQ0FBQyxDQUFDLGNBQUYsQ0FBQTtRQUNBLElBQUMsQ0FBQSxPQUFELENBQVMsVUFBVCxFQUFxQixJQUFDLENBQUEsS0FBdEI7TUFGTSxDQUhWO01BT0EsUUFBQSxFQUFVLFNBQUE7UUFDTixJQUFHLElBQUMsQ0FBQSxLQUFLLENBQUMsUUFBVjtVQUNJLElBQUMsQ0FBQSxHQUFHLENBQUMsUUFBTCxDQUFjLFFBQWQsRUFESjs7TUFETSxDQVBWO0tBRFU7SUFhZCxJQUFJLENBQUMsT0FBTCxHQUFlLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBekIsQ0FDWDtNQUFBLFFBQUEsRUFBVSxrQkFBVjtNQUNBLE9BQUEsRUFBUyxLQURUO01BRUEsU0FBQSxFQUFXLHVCQUZYO01BR0EsU0FBQSxFQUFXLElBQUksQ0FBQyxNQUhoQjtNQUlBLGtCQUFBLEVBQW9CLElBSnBCO01BS0EsTUFBQSxFQUFRO1FBQUEsc0JBQUEsRUFBd0IsY0FBeEI7T0FMUjtNQU1BLFlBQUEsRUFBYyxTQUFDLENBQUQ7UUFDVixDQUFDLENBQUMsY0FBRixDQUFBO1FBQ0EsSUFBQyxDQUFBLE9BQUQsQ0FBUyxlQUFUO01BRlUsQ0FOZDtLQURXO0VBZFUsQ0FBN0I7O0VBMkJBLE9BQU8sQ0FBQyxHQUFSLENBQVksT0FBWjs7RUFFQSxHQUFHLENBQUMsUUFBSixHQUFlLFNBQUMsS0FBRCxFQUFRLE9BQVI7O01BQVEsVUFBVTs7SUFDL0IsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFqQixDQUEwQixLQUExQixFQUFpQyxPQUFqQztFQURhOztFQUlmLEdBQUcsQ0FBQyxlQUFKLEdBQXNCLFNBQUE7V0FDcEIsUUFBUSxDQUFDLE9BQU8sQ0FBQztFQURHOztFQUd0QixHQUFHLENBQUMsRUFBSixDQUFPLE9BQVAsRUFBZ0IsU0FBQTtJQUNkLElBQUcsUUFBUSxDQUFDLE9BQVo7TUFDRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQWpCLENBQUE7TUFDQSxJQUFHLElBQUMsQ0FBQSxlQUFELENBQUEsQ0FBQSxLQUFzQixFQUF6QjtRQUNFLEdBQUcsQ0FBQyxPQUFKLENBQVksYUFBWixFQURGO09BRkY7O0VBRGMsQ0FBaEI7O0VBTUEsR0FBRyxDQUFDLEtBQUosQ0FBQTtBQWxmQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6W251bGxdfQ==
